import React from "react";
import Header from "./components/Header";
import ProfileCover from "./components/ProfileCovers";
import ProfileInfomation from "./components/ProfileInfomation";
import ProfileSkill from "./components/ProfileSkill";
import ProfileInterest from "./components/ProfileInterest";
import ProfileWorks from "./components/ProfileWorks";
import ProfileEducations from "./components/ProfileEducations";
import Footer from "./components/Footer";

function App() {
  return (
    <div>
      <Header />
      <div className="page-content">
        <div>
          <ProfileCover />
          <ProfileInfomation />
          <ProfileSkill />
          <ProfileInterest />
          <ProfileWorks />
          <ProfileEducations />
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default App;
