import React from "react";

export default function InterestDefine() {
  return (
    <div className="row">
      <div className="col-md-6 ml-auto mr-auto">
        <div className="h4 text-center mb-4 title">Interests</div>
        <div className="nav-align-center">
          <ul className="nav nav-pills nav-pills-primary" role="tablist">
            <li className="nav-item">
              <a
                className="nav-link active"
                data-toggle="tab"
                href="#Photography"
                role="tablist"
              >
                <i className="fa fa-bookmark" aria-hidden="true" />
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link"
                data-toggle="tab"
                href="#graphic-design"
                role="tablist"
              >
                <i className="fa fa-film" aria-hidden="true" />
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link"
                data-toggle="tab"
                href="#web-development"
                role="tablist"
              >
                <i className="fa fa-gamepad" aria-hidden="true" />
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}
