import React from "react";
import InterestDefine from "./InterestDefine";
import InterestGame from "./InterestGame";
import InterestMovie from "./InterestMovie";
import InterestSport from "./InterestSport";

export default function index() {
  return (
    <div className="section background-second" id="portfolio">
      <div className="container">
        <InterestDefine />
        <div className="tab-content gallery mt-5">
          <InterestSport />
          <InterestMovie />
          <InterestGame />
        </div>
      </div>
    </div>
  );
}
