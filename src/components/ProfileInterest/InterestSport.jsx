import React from "react";

export default function InterestSport() {
  return (
    <div className="tab-pane active" id="Photography">
      <div className="ml-auto mr-auto">
        <div className="row">
          <div className="col-md-6">
            <div
              className="cc-porfolio-image img-raised"
              data-aos="fade-up"
              data-aos-anchor-placement="top-bottom"
            >
              <a href="#Photography">
                <figure className="cc-effect">
                  <img src="images/soccer.jpg" alt="soccer" />
                  <figcaption>
                    <div className="h4">Recent Sport</div>
                    <p>Soccer</p>
                  </figcaption>
                </figure>
              </a>
            </div>
            <div
              className="cc-porfolio-image img-raised"
              data-aos="fade-up"
              data-aos-anchor-placement="top-bottom"
            >
              <a href="#Photography">
                <figure className="cc-effect">
                  <img src="images/batketboll.jpg" alt="basketball" />
                  <figcaption>
                    <div className="h4">Past Sport</div>
                    <p>Basketball</p>
                  </figcaption>
                </figure>
              </a>
            </div>
          </div>
          <div className="col-md-6">
            <div
              className="cc-porfolio-image img-raised"
              data-aos="fade-up"
              data-aos-anchor-placement="top-bottom"
            >
              <a href="#Photography">
                <figure className="cc-effect">
                  <img src="images/du-lich-vung-tau.jpg" alt="travel" />
                  <figcaption>
                    <div className="h4">Travel</div>
                    <p>Travel Vũng Tàu</p>
                  </figcaption>
                </figure>
              </a>
            </div>
            <div
              className="cc-porfolio-image img-raised"
              data-aos="fade-up"
              data-aos-anchor-placement="top-bottom"
            >
              <a href="#Photography">
                <figure className="cc-effect">
                  <img src="images/du-lich-da-lat.jpg" alt="travel" />
                  <figcaption>
                    <div className="h4">Travel</div>
                    <p>Travel Đà Lạt</p>
                  </figcaption>
                </figure>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
