import React from "react";

export default function InterestMovie() {
  return (
    <div className="tab-pane" id="graphic-design" role="tabpanel">
      <div className="ml-auto mr-auto">
        <div className="row">
          <div className="col-md-6">
            <div
              className="cc-porfolio-image img-raised"
              data-aos="fade-up"
              data-aos-anchor-placement="top-bottom"
            >
              <a href="#graphic-design">
                <figure className="cc-effect">
                  <img src="images/one-piece.jpg" alt="one-piece" />
                  <figcaption>
                    <div className="h4">Anime Movie</div>
                    <p>One Piece Movie</p>
                  </figcaption>
                </figure>
              </a>
            </div>
            <div
              className="cc-porfolio-image img-raised"
              data-aos="fade-up"
              data-aos-anchor-placement="top-bottom"
            >
              <a href="#graphic-design">
                <figure className="cc-effect">
                  <img src="images/naruto.jpg" alt="naruto" />
                  <figcaption>
                    <div className="h4">Anime Movie</div>
                    <p>Naruto Movie</p>
                  </figcaption>
                </figure>
              </a>
            </div>
          </div>
          <div className="col-md-6">
            <div
              className="cc-porfolio-image img-raised"
              data-aos="fade-up"
              data-aos-anchor-placement="top-bottom"
            >
              <a href="#graphic-design">
                <figure className="cc-effect">
                  <img src="images/dead-note.jpg" alt="death-note" />
                  <figcaption>
                    <div className="h4">Anime Movie</div>
                    <p>Death Note Movie</p>
                  </figcaption>
                </figure>
              </a>
            </div>
            <div
              className="cc-porfolio-image img-raised"
              data-aos="fade-up"
              data-aos-anchor-placement="top-bottom"
            >
              <a href="#graphic-design">
                <figure className="cc-effect">
                  <img src="images/one-punch-man.jpg" alt="one-punch-man" />
                  <figcaption>
                    <div className="h4">Anime Movie</div>
                    <p>One Punch Man Movie</p>
                  </figcaption>
                </figure>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
