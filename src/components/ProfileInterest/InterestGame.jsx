import React from "react";

export default function InterestGame() {
  return (
    <div className="tab-pane" id="web-development" role="tabpanel">
      <div className="ml-auto mr-auto">
        <div className="row">
          <div className="col-md-6">
            <div
              className="cc-porfolio-image img-raised"
              data-aos="fade-up"
              data-aos-anchor-placement="top-bottom"
            >
              <a href="#web-development">
                <figure className="cc-effect">
                  <img src="images/LOL-1.jpg" alt="lol-game" />
                  <figcaption>
                    <div className="h4">Recent Game</div>
                    <p>League of Legends</p>
                  </figcaption>
                </figure>
              </a>
            </div>
            <div
              className="cc-porfolio-image img-raised"
              data-aos="fade-up"
              data-aos-anchor-placement="top-bottom"
            >
              <a href="#web-development">
                <figure className="cc-effect">
                  <img src="images/vo-lam-truyen-ky.jpg" alt="vo-lam" />
                  <figcaption>
                    <div className="h4">Past Game</div>
                    <p>Võ Lâm Truyền Kỳ</p>
                  </figcaption>
                </figure>
              </a>
            </div>
          </div>
          <div className="col-md-6">
            <div
              className="cc-porfolio-image img-raised"
              data-aos="fade-up"
              data-aos-anchor-placement="top-bottom"
            >
              <a href="#web-development">
                <figure className="cc-effect">
                  <img src="images/LOL-2.jpg" alt="lol-game" />
                  <figcaption>
                    <div className="h4">Recent Game</div>
                    <p>Teamfight Tactics</p>
                  </figcaption>
                </figure>
              </a>
            </div>
            <div
              className="cc-porfolio-image img-raised"
              data-aos="fade-up"
              data-aos-anchor-placement="top-bottom"
            >
              <a href="#web-development">
                <figure className="cc-effect">
                  <img src="images/Gunny.png" alt="gunny" />
                  <figcaption>
                    <div className="h4">Past Game</div>
                    <p>Gunny</p>
                  </figcaption>
                </figure>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
