import React from "react";
import { FB_LINK, GMAIL_LINK, INS_LINK, GITLAB_LINK } from "../../constant";

const Footer = () => {
  return (
    <footer className="footer">
      <div className="container text-center">
        <a className="cc-facebook btn btn-link" href={FB_LINK}>
          <i className="fa fa-facebook fa-2x " aria-hidden="true" />
        </a>
        <a className="cc-twitter btn btn-link " href={GITLAB_LINK}>
          <i className="fa fa-gitlab fa-2x " aria-hidden="true" />
        </a>
        <a className="cc-google-plus btn btn-link" href={GMAIL_LINK}>
          <i className="fa fa-google-plus fa-2x" aria-hidden="true" />
        </a>
        <a className="cc-instagram btn btn-link" href={INS_LINK}>
          <i className="fa fa-instagram fa-2x " aria-hidden="true" />
        </a>
      </div>
      <div className="h4 title text-center">Tôn Việt Nguyên</div>
      <div className="text-center text-muted">
        <p>
          © ♥ ©
          <br />
          Design - Of - Me
        </p>
      </div>
    </footer>
  );
};

export default Footer;
