import React from "react";

export default function InfomationBasic() {
  return (
    <div className="col-lg-6 col-md-12">
      <div className="card-body">
        <div className="h4 mt-0 title">Basic Information</div>
        <div className="row">
          <div className="col-sm-4">
            <strong className="text-uppercase">Age:</strong>
          </div>
          <div className="col-sm-8">23</div>
        </div>
        <div className="row mt-3">
          <div className="col-sm-4">
            <strong className="text-uppercase">Email:</strong>
          </div>
          <div className="col-sm-8">tvietnguyen1997@gmail.com</div>
        </div>
        <div className="row mt-3">
          <div className="col-sm-4">
            <strong className="text-uppercase">Phone:</strong>
          </div>
          <div className="col-sm-8">0965798122</div>
        </div>
        <div className="row mt-3">
          <div className="col-sm-4">
            <strong className="text-uppercase">Address:</strong>
          </div>
          <div className="col-sm-8">
            24/27B, Phường Tân Đông Hiệp, Dĩ An, Bình Dương
          </div>
        </div>
        <div className="row mt-3">
          <div className="col-sm-4">
            <strong className="text-uppercase">Language:</strong>
          </div>
          <div className="col-sm-8">VietNamese, English</div>
        </div>
      </div>
    </div>
  );
}
