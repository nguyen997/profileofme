import React from "react";

export default function InfomationAbout() {
  return (
    <div className="col-lg-6 col-md-12">
      <div className="card-body">
        <div className="h4 mt-0 title">About</div>
        <p>
          Hello! I'm Tôn Việt Nguyên. I am a programming enthusiast, my main job
          is web development
        </p>
        <p>
          As a young programmer, I want to learn, discover many things, dedicate
          myself to my youth, help and share to develop better people together.
          I want to live a life with no regrets
        </p>
      </div>
    </div>
  );
}
