import React from "react";
import InfomationAbout from "./InfomationAbout";
import InfomationBasic from "./InfomationBasic";

export default function index() {
  return (
    <div className="section background-first " id="about">
      <div className="container">
        <div className="card" data-aos="fade-up" data-aos-offset={10}>
          <div className="row">
            <InfomationAbout />
            <InfomationBasic />
          </div>
        </div>
      </div>
    </div>
  );
}
