import React from "react";
import ProfileCoverImage from "./ProfileCoverImage";
import ProfileCoverLink from "./ProfileCoverLink";

export default function index() {
  return (
    <div className="profile-page">
      <div className="wrapper">
        <div className="page-header page-header-small" filter-color="green">
          <ProfileCoverImage />
          <ProfileCoverLink />
        </div>
      </div>
    </div>
  );
}
