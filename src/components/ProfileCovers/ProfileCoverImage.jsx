import React from "react";

export default function ProfileCoverAbout() {
  return (
    <>
      <div
        className="page-header-image"
        data-parallax="true"
        style={{ backgroundImage: 'url("images/cc-bg-1.jpg")' }}
      />
      <div className="container">
        <div className="content-center">
          <div className="cc-profile-image">
            <a href="#home">
              <img src="images/avarta-of-me.jpeg" alt="avarta" />
            </a>
          </div>
          <div className="h2 title">Tôn Việt Nguyên</div>
          <p className="category text-white">Web Developer</p>
          <a
            className="btn btn-primary smooth-scroll mr-2"
            href="#contact"
            data-aos="zoom-in"
            data-aos-anchor="data-aos-anchor"
          >
            Hire Me
          </a>
          <a
            className="btn btn-primary"
            href="#home"
            data-aos="zoom-in"
            data-aos-anchor="data-aos-anchor"
          >
            Download CV
          </a>
        </div>
      </div>
    </>
  );
}
