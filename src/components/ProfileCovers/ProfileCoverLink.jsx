import React from "react";
import { FB_LINK, GMAIL_LINK, INS_LINK, GITLAB_LINK } from "../../constant";

export default function ProfileCoverLink() {
  return (
    <div className="section">
      <div className="container">
        <div className="button-container">
          <a
            className="cc-facebook btn btn-default btn-round btn-lg btn-icon"
            href={FB_LINK}
            rel="tooltip"
            title="Follow me on Facebook"
          >
            <i className="fa fa-facebook" />
          </a>
          <a
            className="cc-twitter btn btn-default btn-round btn-lg btn-icon"
            href={GITLAB_LINK}
            rel="tooltip"
            title="Follow me on Gitlab"
          >
            <i className="fa fa-gitlab" />
          </a>
          <a
            className="cc-google-plus btn btn-default btn-round btn-lg btn-icon"
            href={GMAIL_LINK}
            rel="tooltip"
            title="Send Gmail to me"
          >
            <i className="fa fa-google-plus" />
          </a>
          <a
            className="cc-instagram btn btn-default btn-round btn-lg btn-icon"
            href={INS_LINK}
            rel="tooltip"
            title="Follow me on Instagram"
          >
            <i className="fa fa-instagram" />
          </a>
        </div>
      </div>
    </div>
  );
}
