import React from "react";

export default function ProfileSkillBE() {
  return (
    <div>
      <div className="row d-flex justify-content-center mb-3">
        <h6>Back-end</h6>
      </div>
      <div className="row">
        <div className="col-md-6">
          <div className="progress-container progress-primary">
            <span className="progress-badge">Nodejs</span>
            <div className="progress">
              <div
                className="progress-bar progress-bar-primary"
                data-aos="progress-full"
                data-aos-offset={10}
                data-aos-duration={2000}
                role="progressbar"
                aria-valuenow={70}
                aria-valuemin={0}
                aria-valuemax={100}
                style={{ width: "70%" }}
              />
              <span className="progress-value">70%</span>
            </div>
          </div>
        </div>
        <div className="col-md-6">
          <div className="progress-container progress-primary">
            <span className="progress-badge">Express</span>
            <div className="progress">
              <div
                className="progress-bar progress-bar-primary"
                data-aos="progress-full"
                data-aos-offset={10}
                data-aos-duration={2000}
                role="progressbar"
                aria-valuenow={70}
                aria-valuemin={0}
                aria-valuemax={100}
                style={{ width: "70%" }}
              />
              <span className="progress-value">70%</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
