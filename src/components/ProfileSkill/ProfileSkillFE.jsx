import React from "react";

export default function ProfileSkillFE() {
  return (
    <div>
      <div className="row d-flex justify-content-center mb-3">
        <h6>Front-end</h6>
      </div>
      <div className="row">
        <div className="col-md-6">
          <div className="progress-container progress-primary">
            <span className="progress-badge">HTML</span>
            <div className="progress">
              <div
                className="progress-bar progress-bar-primary"
                data-aos="progress-full"
                data-aos-offset={10}
                data-aos-duration={2000}
                role="progressbar"
                aria-valuenow={60}
                aria-valuemin={0}
                aria-valuemax={100}
                style={{ width: "80%" }}
              />
              <span className="progress-value">80%</span>
            </div>
          </div>
        </div>
        <div className="col-md-6">
          <div className="progress-container progress-primary">
            <span className="progress-badge">CSS</span>
            <div className="progress">
              <div
                className="progress-bar progress-bar-primary"
                data-aos="progress-full"
                data-aos-offset={10}
                data-aos-duration={2000}
                role="progressbar"
                aria-valuenow={60}
                aria-valuemin={0}
                aria-valuemax={100}
                style={{ width: "75%" }}
              />
              <span className="progress-value">75%</span>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-md-6">
          <div className="progress-container progress-primary">
            <span className="progress-badge">JavaScript</span>
            <div className="progress">
              <div
                className="progress-bar progress-bar-primary"
                data-aos="progress-full"
                data-aos-offset={10}
                data-aos-duration={2000}
                role="progressbar"
                aria-valuenow={85}
                aria-valuemin={0}
                aria-valuemax={100}
                style={{ width: "85%" }}
              />
              <span className="progress-value">85%</span>
            </div>
          </div>
        </div>
        <div className="col-md-6">
          <div className="progress-container progress-primary">
            <span className="progress-badge">TypeScript</span>
            <div className="progress">
              <div
                className="progress-bar progress-bar-primary"
                data-aos="progress-full"
                data-aos-offset={10}
                data-aos-duration={2000}
                role="progressbar"
                aria-valuenow={65}
                aria-valuemin={0}
                aria-valuemax={100}
                style={{ width: "65%" }}
              />
              <span className="progress-value">65%</span>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-md-6">
          <div className="progress-container progress-primary">
            <span className="progress-badge">Bootstrap + Sass</span>
            <div className="progress">
              <div
                className="progress-bar progress-bar-primary"
                data-aos="progress-full"
                data-aos-offset={10}
                data-aos-duration={2000}
                role="progressbar"
                aria-valuenow={90}
                aria-valuemin={0}
                aria-valuemax={100}
                style={{ width: "90%" }}
              />
              <span className="progress-value">90%</span>
            </div>
          </div>
        </div>
        <div className="col-md-6">
          <div className="progress-container progress-primary">
            <span className="progress-badge">React + Redux + React-Router</span>
            <div className="progress">
              <div
                className="progress-bar progress-bar-primary"
                data-aos="progress-full"
                data-aos-offset={10}
                data-aos-duration={2000}
                role="progressbar"
                aria-valuenow={80}
                aria-valuemin={0}
                aria-valuemax={100}
                style={{ width: "80%" }}
              />
              <span className="progress-value">80%</span>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-md-6">
          <div className="progress-container progress-primary">
            <span className="progress-badge">Eslint + Babel</span>
            <div className="progress">
              <div
                className="progress-bar progress-bar-primary"
                data-aos="progress-full"
                data-aos-offset={10}
                data-aos-duration={2000}
                role="progressbar"
                aria-valuenow={75}
                aria-valuemin={0}
                aria-valuemax={100}
                style={{ width: "75%" }}
              />
              <span className="progress-value">75%</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
