import React from "react";
import ProfileSkillFE from "./ProfileSkillFE";
import ProfileSkillBE from "./ProfileSkillBE";

export default function index() {
  return (
    <div className="section background-first " id="skill">
      <div className="container">
        <div className="h4 text-center mb-4 title">Professional Skills</div>
        <div
          className="card"
          data-aos="fade-up"
          data-aos-anchor-placement="top-bottom"
        >
          <div className="card-body">
            <ProfileSkillFE />
            <ProfileSkillBE />
          </div>
        </div>
      </div>
    </div>
  );
}
