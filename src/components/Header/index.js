import React from "react";
import HeaderProfile from "./HeaderProfile";
import HeaderNavigation from "./HeaderNavigation";

export default function index() {
  return (
    <header>
      <div className="profile-page sidebar-collapse">
        <nav
          className="navbar navbar-expand-lg fixed-top navbar-transparent bg-primary"
          color-on-scroll={400}
        >
          <div className="container">
            <HeaderProfile />
            <HeaderNavigation />
          </div>
        </nav>
      </div>
    </header>
  );
}
