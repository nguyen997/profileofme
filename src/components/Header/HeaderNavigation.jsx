import React from "react";

export default function HeaderNavigation() {
  return (
    <div
      className="collapse navbar-collapse justify-content-end"
      id="navigation"
    >
      <ul className="navbar-nav">
        <li className="nav-item">
          <a className="nav-link smooth-scroll" href="#about">
            About
          </a>
        </li>
        <li className="nav-item">
          <a className="nav-link smooth-scroll" href="#skill">
            Skills
          </a>
        </li>
        <li className="nav-item">
          <a className="nav-link smooth-scroll" href="#portfolio">
            Interests
          </a>
        </li>
        <li className="nav-item">
          <a className="nav-link smooth-scroll" href="#experience">
            Experience
          </a>
        </li>
        <li className="nav-item">
          <a className="nav-link smooth-scroll" href="#contact">
            Contact
          </a>
        </li>
      </ul>
    </div>
  );
}
