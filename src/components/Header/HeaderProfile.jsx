import React from "react";
import { Link } from "react-router-dom";

export default function HeaderProfile() {
  return (
    <div className="navbar-translate">
      <Link className="navbar-brand" to="/" rel="tooltip">
        Profile Me
      </Link>
      <button
        className="navbar-toggler navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navigation"
        aria-controls="navigation"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-bar bar1" />
        <span className="navbar-toggler-bar bar2" />
        <span className="navbar-toggler-bar bar3" />
      </button>
    </div>
  );
}
