import React from "react";

export default function index() {
  return (
    <div className="section background-second " id="experience">
      <div className="container cc-experience">
        <div className="h4 text-center mb-4 title">Work Experience</div>
        <div className="card">
          <div className="row">
            <div
              className="col-md-3 bg-primary"
              data-aos="fade-right"
              data-aos-offset={50}
              data-aos-duration={500}
            >
              <div className="card-body cc-experience-header">
                <p>March 2019 - Present</p>
                <div className="h5">Developer Web</div>
              </div>
            </div>
            <div
              className="col-md-9"
              data-aos="fade-left"
              data-aos-offset={50}
              data-aos-duration={500}
            >
              <div className="card-body">
                <div className="h5">Developer Web Asset</div>
                <p>
                  Website developer for real estate ecosystem project Asset,
                  mainly working is programming Front-end and support Back-end
                  for system development
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
