import React from "react";

export default function EducationSchool() {
  return (
    <div className="card">
      <div className="row">
        <div
          className="col-md-3 bg-primary"
          data-aos="fade-right"
          data-aos-offset={50}
          data-aos-duration={500}
        >
          <div className="card-body cc-education-header">
            <p>2012 - 2015</p>
            <div className="h5">High School</div>
          </div>
        </div>
        <div
          className="col-md-9"
          data-aos="fade-left"
          data-aos-offset={50}
          data-aos-duration={500}
        >
          <div className="card-body">
            <div className="h5">The Student</div>
            <p className="category">Nguyễn An Ninh High School</p>
            <p>Is a student of Nguyễn An Ninh High School</p>
            <p>Basic knowledge with pascal subject.</p>
          </div>
        </div>
      </div>
    </div>
  );
}
