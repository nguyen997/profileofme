import React from "react";

export default function EducationUnivercity() {
  return (
    <div className="card">
      <div className="row">
        <div
          className="col-md-3 bg-primary"
          data-aos="fade-right"
          data-aos-offset={50}
          data-aos-duration={500}
        >
          <div className="card-body cc-education-header">
            <p>2015 - PRESENT</p>
            <div className="h5">University</div>
          </div>
        </div>
        <div
          className="col-md-9"
          data-aos="fade-left"
          data-aos-offset={50}
          data-aos-duration={500}
        >
          <div className="card-body">
            <div className="h5">Student University</div>
            <p className="category">
              Industrial University of Ho Chi Minh City
            </p>
            <p>
              I am a student at Ho Chi Minh City University of Industry,
              majoring in electricity - electronics. Knowledge of languages: C,
              C++.Have a background in embedded programming
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
