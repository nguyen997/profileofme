import React from "react";
import EducationUnivercity from "./EducationUnivercity";
import EducationSchool from "./EducationSchool";

export default function index() {
  return (
    <div className="section background-second ">
      <div className="container cc-education">
        <div className="h4 text-center mb-4 title">Education</div>
        <EducationUnivercity />
        <EducationSchool />
      </div>
    </div>
  );
}
