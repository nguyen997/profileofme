import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./App.css";
import * as serviceWorker from "./serviceWorker";
import Snowfall from "react-snowfall";
import { BrowserRouter } from "react-router-dom";

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <App />
      <Snowfall style={{ height: 3000 }} />
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
